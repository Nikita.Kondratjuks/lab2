package INF101.lab2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private final int maxCapacity = 20;
    private List<FridgeItem> storage = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return storage.size();
    }

    @Override
    public int totalSize(){
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            return storage.add(item);
        }
        return false;
    }
    /* w3school java exception
    https://www.w3schools.com/java/java_try_catch.asp
     */
    @Override
    public void takeOut(FridgeItem item){
        if (storage.contains(item)) {
            storage.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }
    @Override
    public void emptyFridge(){
        storage.clear();
    }
    /* w3school java iterator
    https://www.w3schools.com/java/java_iterator.asp
     */
    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFoods = new ArrayList<>(storage);
        expiredFoods.removeIf(item -> !(item.hasExpired()));
        storage.removeIf(item -> item.hasExpired());
        return expiredFoods;
    }
}
/*
{
        List<FridgeItem> expiredItem = new ArrayList<>();
        Iterator<FridgeItem> itemInFridge = storage.iterator();

        while(itemInFridge.hasNext()) {
            FridgeItem item = itemInFridge.next();
            if(item.hasExpired()){
                itemInFridge.remove();
                expiredItem.add(item);
            }
        }
        return expiredItem;

    }
 */
